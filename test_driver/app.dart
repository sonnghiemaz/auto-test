import 'package:flutter_driver/driver_extension.dart';
import 'package:auto_test/main.dart' as app;
void main() {
  enableFlutterDriverExtension();

  app.main();
}
