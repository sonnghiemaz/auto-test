import 'package:auto_test/screens/favorites/favorites_notifier.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Testing App', () {
    var favorites = FavoritesNotifier();

    test('Add new item', () {
      var number = 15;
      favorites.add(number);
      expect(favorites.items.contains(number), true);
    });

    test('Remove an item', () {
      var number = 45;
      favorites.add(number);
      expect(favorites.items.contains(number), true);
      favorites.remove(number);
      expect(favorites.items.contains(number), false);
    });
  });
}
